## This is my AWS VPC name


resource "aws_vpc" "itafader_vpc" {
  cidr_block       = "10.0.0.0/16"

  tags = {
    Name = "itafader_vpc"
  }
}


## This is my public subnet


resource "aws_subnet" "public_subnet" {
  vpc_id     = aws_vpc.itafader_vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "public-subnet"
  }
}


## This is my private subnet


resource "aws_subnet" "private_subnet" {
  vpc_id     = aws_vpc.itafader_vpc.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "private-subnet"
  }
}


