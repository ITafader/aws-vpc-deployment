resource "tls_private_key" "rsa_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}


resource "aws_key_pair" "ec2_key" {
  key_name   = "ec2-key-pair"
  public_key = tls_private_key.rsa_key.public_key_openssh
}




## This will create a key pair name "ec2-key-pair" in AWS.
