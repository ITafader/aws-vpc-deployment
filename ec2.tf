resource "aws_instance" "web" {
  ami           = "ami-0a695f0d95cefc163"
  instance_type = "t3.micro"
  key_name = "ec2-key-pair"
  subnet_id = aws_subnet.public_subnet.id
  associate_public_ip_address = true
  security_groups = [aws_security_group.allow_all.id]
  tags = {
    Name = "Terraform-Server"
  }
}

