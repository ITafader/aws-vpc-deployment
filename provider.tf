## from the provider Terraform
## will create an S3 bucket & a path for state file

terraform {
  backend "s3" {
    bucket = "itafader-terraform-state-bucket"
    key    = "iac/terraform.tfstate"
    region = "us-east-2"
  }

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.60.0"
    }
    tls = {
      source = "hashicorp/tls"
      version = "4.0.4"
    }
  }
}

provider "aws" {
  region = "us-east-2"
 
}


provider "tls" {
}


