resource "aws_eip" "nat_gw_eip" {
  vpc      = true

 tags = {
    Name = "elastic_IP"
  }
}



resource "aws_nat_gateway" "private_subnet_natgateway" {
  allocation_id = aws_eip.nat_gw_eip.id
  subnet_id     = aws_subnet.public_subnet.id

  tags = {
    Name = "nat-gateway"
  }

    depends_on = [aws_internet_gateway.internet-gateway]
}


